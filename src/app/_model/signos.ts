import { Paciente } from "./paciente";

export class Signos {
    idSigno: number;
    paciente: Paciente;
    temperatura: string;
    pulso: string;
    ritmo: string;
    fecha: string;
}
