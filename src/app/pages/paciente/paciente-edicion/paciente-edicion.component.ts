import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import { switchMap, tap } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;
  dialogActive: boolean = false;
  locationHeaderResponse: string = null;

  constructor(
              private route: ActivatedRoute,
              private router: Router,
              private pacienteService: PacienteService,
              @Optional() @Inject(MAT_DIALOG_DATA) public data,
              public dialogRef: MatDialogRef<PacienteEdicionComponent>
            ) {
    if (data != null) {
      this.dialogActive = data.dialog;
    }

  }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'email': new FormControl(''),
      'dni': new FormControl(''),
      'telefono': new FormControl(''),
      'direccion': new FormControl(''),
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
  }

  operar() {
    let paciente = new Paciente();
    paciente.idPaciente = this.form.value['id'];
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.email = this.form.value['email'];
    paciente.dni = this.form.value['dni'];
    paciente.telefono = this.form.value['telefono'];
    paciente.direccion = this.form.value['direccion'];

    if (this.edicion) {
      //MODIFICAR
      //PRACTICA COMUN
      this.pacienteService.modificar(paciente).subscribe(() => {
        this.pacienteService.listar().subscribe(data => {
          this.pacienteService.setPacienteCambio(data);
          this.pacienteService.setMensajeCambio('SE MODIFICÓ');
        });
      });
    } else {
      //REGISTRAR
      //PRACTICA IDEAL
      this.pacienteService.registrar(paciente).pipe(
        tap(resp => {
          if (this.dialogActive) {
            this.locationHeaderResponse = resp.headers.get('location');
            this.dialogRef.close({
              locationResponse: this.locationHeaderResponse
            });
          }
        }),
        switchMap(() => {
        return this.pacienteService.listar();
      })).subscribe(data => {
        this.pacienteService.setPacienteCambio(data);
        this.pacienteService.setMensajeCambio('SE REGISTRO');

      });
    }

    if (!this.dialogActive) {
      this.router.navigate(['/pages/paciente']);
    }

  }

  initForm() {
    if (this.edicion) {
      this.pacienteService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idPaciente),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'email': new FormControl(data.email),
          'dni': new FormControl(data.dni),
          'telefono': new FormControl(data.telefono),
          'direccion': new FormControl(data.direccion),
        });
      });
    }
  }

}
