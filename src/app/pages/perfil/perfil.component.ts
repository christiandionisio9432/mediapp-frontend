import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  public token = sessionStorage.getItem(environment.TOKEN_NAME);
  public decodedToken = null;
  public roles = [];

  constructor() { }

  ngOnInit(): void {
    const helper = new JwtHelperService();
    this.decodedToken = helper.decodeToken(this.token);
    this.decodedToken.authorities.forEach(rol => {
      let color = '';
      if (rol === 'ADMIN') {
        color = 'primary';
      }else if (rol == 'DBA') {
        color = 'accent';
      }else {
        color = 'basic'
      }
      this.roles.push({name: rol, color: color});
    });

    console.log(this.roles);

  }

}
