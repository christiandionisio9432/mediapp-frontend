import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';
import { PacienteEdicionComponent } from '../../paciente/paciente-edicion/paciente-edicion.component';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  id: number;
  form: FormGroup;
  signos: Signos;
  edicion: boolean = false;

  pacientes$: Observable<Paciente[]>;
  idPacienteSeleccionado: number;

  maxFecha: Date = new Date();

  constructor(
    private signosService: SignosService,
    private pacienteService: PacienteService,
    private router: Router,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.signos = new Signos();

    this.iniciarFormulario();

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.iniciarFormularioEditar();
    });

    this.pacienteService.getPacienteCambio().subscribe(data => {
      this.listarPacientes();
    });

    this.pacienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.listarPacientes();

  }

  private iniciarFormulario() {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
      'fecha': new FormControl('', Validators.required)
    });
  }

  iniciarFormularioEditar() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        let id = data.idSigno;
        let paciente = data.paciente;
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmo = data.ritmo;
        let fecha = data.fecha;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente': new FormControl(paciente),
          'temperatura': new FormControl(temperatura),
          'pulso': new FormControl(pulso),
          'ritmo': new FormControl(ritmo),
          'fecha': new FormControl(fecha, Validators.required)
        });

        this.idPacienteSeleccionado = data.paciente.idPaciente;
      });
    }
  }

  operar() {

    const {fecha} = this.form.value;
    this.form.controls['fecha'].setValue(this.formatearFecha(fecha));

    let paciente = new Paciente();
    paciente.idPaciente = this.idPacienteSeleccionado;
    this.form.controls['paciente'].setValue(paciente);

    this.signos.idSigno = this.form.value['id'];
    this.signos.paciente = this.form.value['paciente'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmo = this.form.value['ritmo'];
    this.signos.fecha = this.form.value['fecha'];

    if (this.edicion) {
      this.signosService.modificar(this.signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe( (data) => {
        this.signosService.setSignoCambio(data);
        this.signosService.setMensajeCambio("Se modificó coreectamente");
        this.router.navigate(['/pages/signos']);
      });
    } else {
      this.signosService.registrar(this.signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe( (data) => {
        this.signosService.setSignoCambio(data);
        this.signosService.setMensajeCambio("Se registró coreectamente");
        this.router.navigate(['/pages/signos']);
      });
    }

  }

  listarPacientes() {
    this.pacientes$ = this.pacienteService.listar();
  }

  formatearFecha(fecha: string) {
    return moment(fecha).format('YYYY-MM-DDTHH:mm:ss');
  }

  abrirDialog() {
    this.dialog.open(PacienteEdicionComponent, {
        data: {
          dialog: true
        }
    }).afterClosed().subscribe(response => {
      const id = response.locationResponse.split('pacientes/')
      this.setPacienteComboBox(id[1]);

    });
  }
  setPacienteComboBox(id: string) {
    this.pacienteService.listarPorId(Number(id)).subscribe( data => {
      let paciente = new Paciente();
      paciente = data;
      this.idPacienteSeleccionado = paciente.idPaciente;
    })
  }

}
